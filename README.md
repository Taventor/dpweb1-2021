# dpweb1-2021

Publicaciones de clase

## Acerca de

Este proyecto sirve para evidenciar el trabajo en la asignatura de _Desarrollo de la plataforma web I_ de la _**Universidad Tecnológica de El Salvador**_ ciclo II del año 2021.

Los ejercicios donde puede verse el desarrollo de los contenidos se publica [Este sitio web](http://2700462015.orbenit.com).

## Temáticas.

* HTML5.
* CSS.
* JavaScript.
* PHP.
* MySQL o MariaDB.
* Servidores Web.

