/* document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("form-registro").addEventListener("submit", validarForm);
});


function validarForm(evento) {
    evento.preventDefault();
    var nickname = document.getElementById('tb_nombre').value;
    if (nickname == null || nickname.length() == 0 || isNaN(nickname)) {
        document.getElementById('msg=nombre').value('Este campo no puede estar vacío <i class="bi bi-alarm"></i>');
        alert('Error, no seas estupido...');
        return;
    }
    this.submit();
} */

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function(form) {
            form.addEventListener('submit', function(event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
        })
})()