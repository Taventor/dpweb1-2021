const iconMenu = document.querySelector('#btn-menu');
const menu = document.querySelector('.menu');
const links = document.querySelectorAll('li');

iconMenu.addEventListener('click', () => {
    menu.classList.toggle('open');
});

// Activapopovers en la página
var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function(popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl)
})