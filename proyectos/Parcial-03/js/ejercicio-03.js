const tituloRes3 = document.getElementById('ttlRes3')
const Res3 = document.getElementById('res3')
const pat3 = document.getElementById('pataRespuesta3')
    /* mañana */
var ma1 = document.getElementById('ea1m')
var ma2 = document.getElementById('ea2m')
var ma3 = document.getElementById('ea3m')
var ma4 = document.getElementById('ea4m')
var ma5 = document.getElementById('ea5m')
    /* tarde */
var ta1 = document.getElementById('ea1t')
var ta2 = document.getElementById('ea2t')
var ta3 = document.getElementById('ea3t')
var ta4 = document.getElementById('ea4t')
var ta5 = document.getElementById('ea5t')
var ta6 = document.getElementById('ea6t')
    /* noche */
var na1 = document.getElementById('ea1n')
var na2 = document.getElementById('ea2n')
var na3 = document.getElementById('ea3n')
var na4 = document.getElementById('ea4n')
var na5 = document.getElementById('ea5n')
var na6 = document.getElementById('ea6n')
var na7 = document.getElementById('ea7n')

function promedioEdades() {
    /* conversiones */
    var m1 = parseInt(ma1.value)
    var m2 = parseInt(ma2.value)
    var m3 = parseInt(ma3.value)
    var m4 = parseInt(ma4.value)
    var m5 = parseInt(ma5.value)
    var t1 = parseInt(ta1.value)
    var t2 = parseInt(ta2.value)
    var t3 = parseInt(ta3.value)
    var t4 = parseInt(ta4.value)
    var t5 = parseInt(ta5.value)
    var t6 = parseInt(ta6.value)
    var n1 = parseInt(na1.value)
    var n2 = parseInt(na2.value)
    var n3 = parseInt(na3.value)
    var n4 = parseInt(na4.value)
    var n5 = parseInt(na5.value)
    var n6 = parseInt(na6.value)
    var n7 = parseInt(na7.value)
        /* boleanos */
    var v1 = (m1 > 6 && m2 > 6 && m3 > 6 && m4 > 6 && m5 > 6)
    var v2 = (t1 > 6 && t2 > 6 && t3 > 6 && t4 > 6 && t5 > 6 && t6 > 6)
    var v3 = (n1 > 6 && n2 > 6 && n3 > 6 && n4 > 6 && n5 > 6 && n6 > 6 && n7 > 6)
    console.log(v1, v2, v3);
    if (v1 == true && v2 == true && v3 == true) {
        var pm = (m1 + m2 + m3 + m4 + m5) / 5
        var pt = (t1 + t2 + t3 + t4 + t5 + t6) / 6
        var pn = (n1 + n2 + n3 + n4 + n5 + n6 + n7) / 7
        var e1 = "La edad promedio de la mañana es de: " + pm + "<br>"
        var e2 = "La edad promedio de la tarde es de: " + pt + "<br>"
        var e3 = "La edad promedio de la noche es de: " + pn + "<br>"
        var mayor = Math.max(pm, pt, pn)
        var e4 = "Siendo el promedio de edad mas alto: " + mayor
        var e = e1 + e2 + e3 + e4
    }
    tituloRes3.classList.add('text-center')
    tituloRes3.textContent = 'Promedio de edades por turno.'
    Res3.classList.add('text-center')
    Res3.innerHTML = e
    pat3.classList.remove('visually-hidden')
}