const tituloRes4 = document.getElementById('ttlRes4')
const Res4 = document.getElementById('res4')
const pat4 = document.getElementById('pataRespuesta4')

var rel1 = document.getElementById('pi1')
var rel2 = document.getElementById('pi2')
var rel3 = document.getElementById('pi3')
var rel4 = document.getElementById('pi4')
var rel5 = document.getElementById('pi5')

function paresImpares() {
    var re1 = parseInt(rel1.value)
    var re2 = parseInt(rel2.value)
    var re3 = parseInt(rel3.value)
    var re4 = parseInt(rel4.value)
    var re5 = parseInt(rel5.value)

    var validat = (re1 > 0 && re2 > 0 && re3 > 0 && re4 > 0 && re5 > 0)
    if (validat == true) {
        if (re1 % 2 == 0) {
            var s1 = re1 + " es un número par<br>"
        } else {
            var s1 = re1 + " es un número impar<br>"
        }
        if (re2 % 2 == 0) {
            var s2 = re2 + " es un número par<br>"
        } else {
            var s2 = re2 + " es un número impar<br>"
        }
        if (re3 % 2 == 0) {
            var s3 = re3 + " es un número par<br>"
        } else {
            var s3 = re3 + " es un número impar<br>"
        }
        if (re4 % 2 == 0) {
            var s4 = re4 + " es un número par<br>"
        } else {
            var s4 = re4 + " es un número impar<br>"
        }
        if (re5 % 2 == 0) {
            var s5 = re5 + " es un número par<br>"
        } else {
            var s5 = re5 + " es un número impar<br>"
        }
        var salida = s1 + s2 + s3 + s4 + s5
        tituloRes4.classList.add('text-center')
        tituloRes4.textContent = 'Números pares e impares.'
        Res4.classList.add('text-center')
        Res4.innerHTML = salida
        pat4.classList.remove('visually-hidden')
    }
}