const tituloRes2 = document.getElementById('ttlRes2')
const Res2 = document.getElementById('res2')
const pat2 = document.getElementById('pataRespuesta2')
const nom = document.getElementById('nombre')
const vejez = document.getElementById('edad')

function diasVividos() {
    if (nom.value != "" && parseInt(vejez.value) > 0) {
        tituloRes2.classList.add('text-center')
        tituloRes2.textContent = 'Respuesta'
        Res2.classList.add('text-center')
        Res2.textContent = nom.value + "has vivido " + parseInt(vejez.value) * 365 + " días."
        pat2.classList.remove('visually-hidden')
    }
}