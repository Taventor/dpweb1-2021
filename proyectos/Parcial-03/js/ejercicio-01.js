const tituloRes = document.getElementById('ttlRes')
const Res = document.getElementById('res')
const pat = document.getElementById('pataRespuesta')
const nu1 = document.getElementById('n01')
const nu2 = document.getElementById('n02')


function ejercicio1() {
    if (nu1.value != "" && nu2.value != "") {
        tituloRes.classList.add('text-center')
        tituloRes.textContent = 'Respuesta'
        var num1 = parseInt(nu1.value)
        var num2 = parseInt(nu2.value)
        var exp2 = Math.pow((num1 * num2), 2)
        var result1 = (num1 + 5) * (num2 / 3)
        var exp3 = Math.pow((result1 + exp2), 3)
        Res.classList.add('text-center')
        Res.textContent = exp3
        pat.classList.remove('visually-hidden')
    }
}

function limpieza() {
    location.reload();
}